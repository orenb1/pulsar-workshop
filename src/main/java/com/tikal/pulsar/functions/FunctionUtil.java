package com.tikal.pulsar.functions;

import com.tikal.pulsar.TopicsKt;
import com.tikal.pulsar.config.PulsarConfig;
import com.tikal.pulsar.messages.TextMessage;
import java.security.SecureRandom;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;

public class FunctionUtil {

  private static final String[] KEYS = {"A", "B", "C"};

  public static void main(String[] args) throws PulsarClientException, InterruptedException {
    PulsarClient client = new PulsarConfig().getPulsarClient();
    Producer<TextMessage> producer = client.newProducer(Schema.JSON(TextMessage.class))
        .topic(TopicsKt.EX2_SOURCE)
        .create();

    Thread thread = null;
    try {
      for (String key : KEYS) {
        thread = new Thread(new MessageSender(producer, key));
        thread.start();
      }

      thread.join();
    } finally {
      client.shutdown();
    }
  }

  @AllArgsConstructor
  static class MessageSender implements Runnable {
    Producer<TextMessage> producer;
    String key;

    @SneakyThrows
    @Override
    public void run() {
      SecureRandom random = new SecureRandom();

      while(true) {
        String msg = UUID.randomUUID().toString();
        System.out.println("Sending key=" + key  + ", msg=" + msg);
        producer.newMessage()
            .key(key)
            .value(new TextMessage())
            .send();

        Thread.sleep(random.nextInt(1000));
      }
    }
  }
}
