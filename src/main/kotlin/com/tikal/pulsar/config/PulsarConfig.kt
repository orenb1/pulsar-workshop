package com.tikal.pulsar.config;

import org.apache.pulsar.client.api.PulsarClient

class PulsarConfig (serviceUrl: String = "pulsar://localhost:6650") {

    val pulsarClient: PulsarClient = PulsarClient.builder()
            .serviceUrl(serviceUrl)
            .build()

    fun shutdown() {
        pulsarClient.close()
    }
}