package com.tikal.pulsar

const val TOPIC_PREFIX = "persistent://tikal/workshop/"
const val EX_CODE = "${TOPIC_PREFIX}ex.code"

const val EX1_SOURCE = "${TOPIC_PREFIX}ex1.source"
const val EX2_SOURCE = "${TOPIC_PREFIX}ex2.source"

const val EX1_SINK = "${TOPIC_PREFIX}ex1.sink"
const val EX2_SINK = "${TOPIC_PREFIX}ex2.sink"
