package com.tikal.pulsar.messages

data class TextMessage (val message: String) {

    constructor(): this("")

}

