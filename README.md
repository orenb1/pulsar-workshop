# Pulsar workshop

Few exercises which help you to get started with apache-pulsar.  
See http://pulsar.apache.org/docs/en/standalone/  

#### Setup
in project directory, execute `docker-compose up`.

#### Initial run 
_(First time running docker-compose only)_.

Add admin user to pulsar management console:
```
CSRF_TOKEN=$(curl http://localhost:7750/pulsar-manager/csrf-token)
curl \
  -H 'X-XSRF-TOKEN: $CSRF_TOKEN' \
  -H 'Cookie: XSRF-TOKEN=$CSRF_TOKEN;' \
  -H "Content-Type: application/json" \
  -X PUT http://localhost:7750/pulsar-manager/users/superuser \
  -d '{"name": "tikal", "password": "pulsar", "description": "test", "email": "username@test.org"}'
```

#### Management console
##### Login
In a browser, go to `http://localhost:9527`, and use credentials added on _initial run_ step (_tikal/pulsar_).
##### Add environment (Initial run only)
Click on **New Environment**, fill in a name. For **Service URL**, fill in `http://pulsar:8080`, and click **Confirm**.
* `pulsar` used in the URL is the name of the pulsar-stnadalone service, as named in `docker-compose.yml`.

#### Using pulsar client, and admin client via CLI
Since we use standalone with docker, client & admin executables are found within the container itself.
In order to reduce annoying repeating commands, `client.sh` and `admin.sh` can be used instead (which simply delegate arguments to the executables in the container)

##### Topic schema: `[non-]perfsistent://<tenant>/<workspace>/<topic>`

## Exercises
CLI:
* Add a tenant named _Tikal_.
* Add a workspace for this tenant.
* Produce 33 "Hello Pulsar" messages to topic `ex.hello`.
* Consume all messages from topic `ex.hello`.
    * Consume all messages ever sent to topic `ex.hello`.

Code:  
(Use _PulsarConfig.pulsarClient_) 
* Create a consumer and consume (print) messages from topic `ex.code` and run it.
* Create a producer, and send 33 messages to topic `ex.code`.
    * You should see those messages in the consumer's stdout.

Functions:  
* Create a function (implement `org.apache.pulsar.functions.api.Function`), which \`takes\` and \`returns\` `TextMessage`. Use `context.incrCounter` to count messages, and add the 
currently total processed messages count to the message text.
    * Build a jar file using `mvnw package`
    * Deploy to pulsar:  
    `admin.sh functions create --function-config-file data/functions-config/count-func.yaml --classname <function-classname>`
    * Create a CLI consumer that consumes from `persistent://tikal/workshop/ex1.sink`.  
    * Send few messages (`TextMessage`) to topic `persistent://tikal/workshop/ex1.source`. You should see modified messages in the consumer output.  
    
* Create a window-function (implement `org.apache.pulsar.functions.api.WindowFunction`), 
which \`takes\` `TextMessage` and returns a string that describes how many messages were of key `A`, `B`, `C`. (ie `A=#, B=#, C=#`)
    * Build a jar file using `mvnw package`
    * Deploy to pulsar:
     `admin.sh functions create --function-config-file data/functions-config/key-counter.yaml --window-length-duration-ms 5000 --sliding-interval-duration-ms 1000 --classname <function-classname>`
    * Consume from topic `persistent://tikal/workshop/ex2.sink` (CLI)
        * Use `FunctionUtil` to produce messages.